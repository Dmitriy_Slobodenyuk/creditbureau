<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container-fluid">
    <div class="row-fluid">
        <div id="item_error" class="span12"> 
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="message_error" class="span3"></div>
                    <div class="span8" style="margin-top: 15px">
                        <ul style="list-style: none">
                            <li>
                                <i>${message1}</i>
                            </li>
                            <li>
                                <i>${message2}</i>
                            </li>
                        </ul>
                    </div>
                    <div class="span1"></div>
                </div>
            </div>
        </div>
    </div>
</div>