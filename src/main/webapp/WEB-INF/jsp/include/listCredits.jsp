<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach items="${listActiveCredits}" var="credit">

    <div id="credit_item" class="row">
        <div class="span6">
            <span id="bnk">Банк : ${credit.bank.bankName}</span><br>
            <span>Дата выдачи: ${credit.dateIssueToString}</span><br>
            <span>Дата погашения: ${credit.dateEndToString}</span><br>
            <span>Валюта : ${credit.currency.currName}</span><br>
            <span>Тело кредита = ${credit.creditCurrBody}</span><br>
            <span>Просрочки : ${credit.outDelay}</span>
        </div>
        <div class="span6">
            <div class="crd_img"> 
            </div>
            <button id="btn_credit_edit" class="btn btn-primary" onclick="editCredit('${credit.idCredit}',
                            '${credit.creditCurrBody}', '${credit.creditEnd}', '${credit.outDelay}'
                            )">Редактировать</button>
        </div>
    </div>
</c:forEach>