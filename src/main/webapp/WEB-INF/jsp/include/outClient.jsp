<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach items="${listClient}" var="client">

    <div class="container-fluid">
        <div class="row-fluid">
            <div id="itm" class="span12">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div id="item_photo" class="span6" >                                                        
                        </div>
                        <div class="span6">
                            <label class="lbl_item">ФИО : ${client.firstName} ${client.secondName} ${client.lastName}</label>
                            <label class="lbl_item">Дата рождения : ${client.dateBornToString}</label>
                            <label class="lbl_item">ИНН : ${client.inn}</label>
                            <label class="lbl_item">Паспорт : ${client.serialPassport} ${client.numberPassport}</label>

                            <button id="btn_all" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#allModal">Все Кредиты</button>

                            <div id="allModal" class="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="background-color: #f2f2f2">
                                        <div class="modal-header " style="background-color: #0480be">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel" style="color: white">Кредиты клиента</h4>
                                        </div>
                                        <div class="modal-body">
                                            <%@ include file="credit_dialog.jsp" %> 
                                            <div id="oper"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:forEach>