<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach items="${client.listCredits}" var="credit">

    <div id="credit_item" class="row">
        <div class="span6">
            <span>Банк : ${credit.bank.bankName}</span><br>
            <span>Дата выдачи: ${credit.dateIssueToString}</span><br>
            <span>Дата погашения: ${credit.dateEndToString}</span><br>
            <span>Валюта : ${credit.currency.currName}</span><br>
            <span>Тело кредита = ${credit.creditCurrBody}</span><br>
            <span>Просрочки : ${credit.outDelay}</span>
        </div>
        <div class="span6">
            <div class="crd_img"> 
            </div>
        </div>
    </div>
</c:forEach>