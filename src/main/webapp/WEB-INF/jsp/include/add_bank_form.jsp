<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="form_add_bank" method="post" class="form-inline">
    <input class="input-large" type="text" name="bank_name" placeholder="Название банка"><br>
    <input class="input-large" type="text" name="bank_code" placeholder="Код банка (6 лат. символов)"><br>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>
<span id="status_bank"></span>
