<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css.map"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css.map"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/find_client.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/navbar.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/foradmin.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/modal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/popover.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/transition.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>