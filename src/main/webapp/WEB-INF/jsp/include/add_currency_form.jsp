<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="form_add_curr" method="post" class="form-inline">
    <input class="input-large" type="text" name="curr_name" placeholder="Название валюты"><br>
    <input class="input-large" type="text" name="curr_code" placeholder="Код валюты (3 лат. символа)"><br>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>
<span id="status_curr"></span>