<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container-fluid">

    <div class="row-fluid">
        <!--   Block adding and editing credits -->
        <div id='form_credit' class="span5">
            <div class="tabbable" style="margin: 5px;"> 
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab3" data-toggle="tab">Редактировать</a></li>
                    <li><a href="#tab4" data-toggle="tab">Добавить</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab3">
                        <input class="span6" type="text" name="ed_body" placeholder="Тело кредита"><span id="id_sp" style="visibility: hidden"></span><br>
                        <label class="lb">Дата погашения :</label>
                        <input id="input_date" class="span6" type="text" name="ed_date_end" placeholder="ГГГГ-ДД-ММ"><span id="date_credit_edit" class="message"></span><br>
                        <label class="lb">Просрочки :</label>
                        <select class="span6" name="ed_delay">
                            <option>Да</option>
                            <option>Нет</option>
                        </select><br>

                        <button type="submit" class="btn btn-primary" onclick="saveModifiedCredit()">
                            Сохранить</button>
                    </div>  

                    <div class="tab-pane" id="tab4">
                        <label class="lb">Банк :<span id="id_cl" style="visibility: hidden">${client.idClient}</span></label>
                        <select class="span6" name="add_bank">
                            <c:forEach items="${listBanks}" var="bank">
                                <option>${bank.bankName}</option>
                            </c:forEach>

                        </select><br>
                        <label class="lb">Валюта :</label>
                        <select class="span6" name="add_curr"> 
                            <c:forEach items="${listCurrency}" var="currency">
                                <option>${currency.currName}</option>
                            </c:forEach>

                        </select><br>
                        <input class="span6" type="text" name="add_bsumm" placeholder="Взнос"><br>
                        <label class="lb">Дата оформления :</label>
                        <input class="span6" type="text" name="add_begin_date" placeholder="ГГГГ-ДД-ММ"><span id="start_date_edit" class="message"></span><br>
                        <label class="lb">Дата погашения :</label>
                        <input class="span6" type="text" name="add_end_date" placeholder="ГГГГ-ДД-ММ"><span id="end_date_edit" class="message"></span><br>
                        
                        <button type="submit" class="btn btn-primary" onclick="saveNewCredit()">Добавить</button>

                    </div>
                </div>
            </div>
        </div>
        <!--  end of block-->

        <!--  beginning of the block list the loans-->
        <div id="list_credit" class="span7">

            <div class="tabbable" style="margin: 5px"> 
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab5" data-toggle="tab">Активные кредиты</a>
                    </li>
                    <li>
                        <a href="#tab6" data-toggle="tab">Все кредиты</a>
                    </li>
                </ul>
                <div id ="lst_credit" class="tab-content">
                    <div class="tab-pane active" id="tab5">
                        <div id="list_credit_div">
                            <%@ include file="listCredits.jsp" %>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <div id="credit_all">
                            <%@ include file="listAllCredits.jsp" %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

