<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<div class="blog-masthead">
    <div class="container">
        <nav class="blog-nav">
            <a id="nvb_main" class="blog-nav-item active" href="#">Главная</a>
            <a id="nvb_in" class="blog-nav-item" href="#">Поиск клиентов</a>
            <a id="nvb_admin" class="blog-nav-item" href="#">Панель администратора</a>
            <a class="blog-nav-item" href="#">Контакты</a>
        </nav>
    </div>
</div>
