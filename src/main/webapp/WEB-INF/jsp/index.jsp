<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="include/styles.jsp" %>
        <title>Бюро Кредитных историй</title>
    </head>
    <body class="body_text">   
        <%@ include file="include/navbar.jsp" %>
        <%@ include file="include/cap_site.jsp" %>
        <div class="main-body">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span5">
                        <%@ include file="include/text-index.jsp" %>                
                    </div>
                    <div id="index_img" class="span7">
                        <img> 
                    </div>
                </div>
            </div>
        </div>

        <div id="do" class="container-fluid">
            <div class="row-fluid">
                <div class="span4">
                    <img id="left_icon"></img>
                    <p>Поиск клиентов по идентификационному коду , паспортным данным или ФИО.
                        Услуга для банков и организаций "Кредитные истории" — это одновременное отслеживание изменений 
                        кредитных историй субьектов путём регулярного 
                        исследования базы даных бюро.
                    </p>                    
                </div>
                <div class="span4">
                    <img id="middle_icon"></img>
                    <p>Просмотр, редактирование или добавление кредитов.
                        Предоставление услуг, связанных с обработкой и анализом информации, которая составляет кредитную историю,
                        сбор, обработка, использование информации, которая составляет кредитную историю,
                        предоставление кредитных отчетов,
                        хранение информации, составляющей кредитную историю.
                    </p>
                </div>
                <div class="span4">
                    <img id="right_icon"></img>
                    <p>
                        Информация о кредитной истории заемщика передается в Украинское бюро кредитных историй на добровольной основе и только при наличии письменного согласия заемщика.
                    </p>
                </div>
            </div>
        </div>   
        <%@ include file="include/footer.jsp" %>
    </body>
</html>
