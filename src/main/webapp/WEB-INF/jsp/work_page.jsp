<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="include/styles.jsp" %>
        <title>Бюро Кредитных историй</title>
    </head>
    <body class="body_text">
        <%@ include file="include/navbar.jsp" %>

        <div class="container-fluid">

            <div class="row-fluid">
                <!--   Block Search customers -->
                <div id='form_find' class="span5">
                    <div class="tabbable" style="margin: 5px;"> 
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Поиск клиентов</a></li>
                            <li><a href="#tab2" data-toggle="tab">Добавление клиента</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <label class="lb">По ФИО и Дате Рождения:</label>
                                <form id="form_fio" method="post">
                                    <input class="input-medium" type="text" name="fname" placeholder="Имя"><br>
                                    <input class="input-medium" type="text" name="sname" placeholder="Фамилия"><br>
                                    <input class="input-medium" type="text" name="lname" placeholder="Отчество (не обяз.)"><br>                                    
                                    <input class="input-medium" type="text" name="bdate" placeholder="ГГГГ-ДД-ММ"><span id="error_date" class="message"></span><br>
                                    <button type="submit" class="btn btn-primary">Найти</button>
                                </form>

                                <label class="lb">По ИНН:</label>
                                <form id="form_inn" method="post" class="form-inline">
                                    <input id="inn" type="text" name="inn_inp" placeholder="Введите инн (10 цифр)…">
                                    <button type="submit" class="btn btn-primary">Найти</button>
                                </form>

                                <label class="lb">По данным паспорта:</label>
                                <form id="form_passport" method="post" class="form-inline">
                                    <input class="input-small" type="text" name="s_pass" placeholder="серия">
                                    <input class="input-medium" type="text" name="n_pass" placeholder="номер (6 цифр)">
                                    <button type="submit" class="btn btn-primary">Найти</button>
                                </form>
                            </div>  
                            <div class="tab-pane" id="tab2">
                                <label class="lb">Заполните форму :</label>
                                <form id="form_add_client" method="POST">
                                    <input class="input-medium" type="text" name="fName" placeholder="Имя"><br>
                                    <input class="input-medium" type="text" name="sName" placeholder="Фамилия"><br>
                                    <input class="input-medium" type="text" name="lName" placeholder="Отчество (не обяз.)"><br>
                                    <label class="lb">Дата рождения :</label>
                                    <input class="input-medium" type="text" name="dBorn" placeholder="ГГГГ-ДД-ММ"><span id="error_date_add" class="message"></span><br>
                                    <input class="input-medium" type="text" name="inn" placeholder="ИНН (10 цифр)"><br>
                                    <label class="lb">Паспортные данные :</label>
                                    <div class="form-inline">
                                        <input class="input-small" type="text" name="sPass" placeholder="серия">
                                        <input class="input-medium" type="text" name="nPass" placeholder="номер (6 цифр)">
                                    </div><br>
                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  end of the block search for clientsв-->

                <!--  beginning of the block display a list of clients-->
                <div id="find_list" class="span7">
                    <div class="tabbable" style="margin: 5px"> 
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Список клиентов</a>
                            </li>
                        </ul>
                        <div id="lst_client" class="tab-content"  style="">
                            <div class="tab-pane active" id="tab1">
                                <div id="list_client">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end of the block display a list of clients-->
    <%@ include file="include/footer.jsp" %>
    <script>
        $(function () {
            $('.js-popover').popover();
            $('.js-tooltip').tooltip();
        });
    </script>
</body>
</html>
