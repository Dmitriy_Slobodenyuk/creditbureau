<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="include/styles.jsp" %>
        <title>Бюро Кредитных историй</title>
    </head>
    <body>
        <%@ include file="include/navbar.jsp" %>

        <div class="container-fluid" style="padding-bottom: 20px;">

            <div class="row-fluid">
                <!--   Block Search customers -->
                <div id='form_find' class="span6">
                    <div class="tabbable" style="margin: 5px"> 
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Список банков</a>
                            </li>
                        </ul>
                        <div class="tab-content"  style="background-color: white;overflow-y: scroll;height: 375px">
                            <div class="tab-pane active" id="tab1">
                                <div id="list_bank">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid" style="margin-top: 15px;">
                        <div class="row-fluid">
                            <button id="btn_load_bank" class="btn btn-primary btn-lg span6" >Загрузить список банков</button>
                            <button id="btn_add_bank" class="btn btn-primary btn-lg span6" data-toggle="modal" data-target="#addbank" >Добавить банк</button>
                        </div>
                    </div>
                </div>
                <div id='form_find' class="span6">
                    <div class="tabbable" style="margin: 5px"> 
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Список валют</a>
                            </li>
                        </ul>
                        <div class="tab-content"  style="background-color: white;overflow-y: scroll;height: 375px">
                            <div class="tab-pane active" id="tab1">
                                <div id="list_currency">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid" style="margin-top: 15px;">
                        <div class="row-fluid">
                            <button id="btn_load_cur" class="btn btn-primary btn-lg span6" >Загрузить список валют</button>
                            <button id="btn_add_curr" class="btn btn-primary btn-lg span6" data-toggle="modal" data-target="#addcurr" >Добавить валюту</button>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div id="addbank" class="span6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="background-color: #f2f2f2">
                                        <div class="modal-header " style="background-color: #0480be">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel" style="color: white">Добавить банк</h4>
                                        </div>
                                        <div class="modal-body">
                                            <%@ include file="include/add_bank_form.jsp" %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="addcurr" class="span6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="background-color: #f2f2f2">
                                        <div class="modal-header " style="background-color: #0480be">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel" style="color: white">Добавить валюту</h4>
                                        </div>
                                        <div class="modal-body">
                                            <%@ include file="include/add_currency_form.jsp" %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  end of the block display a list of clients-->
        <%@ include file="include/footer.jsp" %>
    </body>
</html>