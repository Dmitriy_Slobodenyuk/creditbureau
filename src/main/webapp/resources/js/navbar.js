$(document).ready(function () {
   // переход на главную страничку
    $("#nvb_main").click(function () {
        var url = "/CreditBH/";
        window.location.href = url;
    });

    // переход на страницу поиска
    $("#nvb_in").click(function () {
        var url = "/CreditBH/in";
        window.location.href = url;
    });

    // переход на страницу администрирования
    $("#nvb_admin").click(function () {
        var url = "/CreditBH/admin_page";
        window.location.href = url;
    });
});
