$(document).ready(function () {

    // get a list banks
    btn_get_banks = function () {
        var url = '/CreditBH/admin/getListBanks';
        var bank_posting = $.get(url);
        bank_posting.done(function (data) {
            var content = data;
            createListBanks(content);
        });
    };

    // get a list currencies
    btn_get_currency = function () {
        var url = '/CreditBH/admin/getCurrency';
        var bank_posting = $.get(url);
        bank_posting.done(function (data) {
            var content = data;
            createListCurrencies(content);
        });
    };

    // display a list banks
    function createListBanks(content) {
        $("#list_bank").empty();
        $.each(content, function (index, value) {
            var container = $("<div></div>").addClass("container-fluid");
            var row = $("<div></div>").addClass("row-fluid");
            var span = $("<div id='item_bank'></div>").addClass("span12");
            var in_container = $("<div></div>").addClass("container-fluid");
            var in_row = $("<div></div>").addClass("row-fluid");
            var in_span = $("<div></div>").addClass("span12");

            var labelName = $("<label>Название банка: " + value.bankName + "</label>");
            labelName.addClass("lbl_item");

            var labelCode = $("<label id='bank_code'>Код банка: " + value.bankCode + "</label>");
            labelCode.addClass("lbl_item cd");

            in_span.append(labelName);
            in_span.append(labelCode);
            in_row.append(in_span);
            in_container.append(in_row);
            span.append(in_container);
            row.append(span);
            container.append(row);
            $("#list_bank").append(container);
        });
    }

    // display a list currencies
    function createListCurrencies(content) {
        $("#list_currency").empty();
        $.each(content, function (index, value) {
            var container = $("<div></div>").addClass("container-fluid");
            var row = $("<div></div>").addClass("row-fluid");
            var span = $("<div id='item_cur'></div>").addClass("span12");
            var in_container = $("<div></div>").addClass("container-fluid");
            var in_row = $("<div></div>").addClass("row-fluid");
            var in_span = $("<div></div>").addClass("span12");

            var labelName = $("<label>Название валюты: " + value.currName + "</label>");
            labelName.addClass("lbl_item");

            var labelCode = $("<label id='currCode'>Код валюты: " + value.currCode + "</label>");
            labelCode.addClass("lbl_item");

            in_span.append(labelName);
            in_span.append(labelCode);
            in_row.append(in_span);
            in_container.append(in_row);
            span.append(in_container);
            row.append(span);
            container.append(row);
            $("#list_currency").append(container);
        });
    }

    //button's handler "Загрузить список банков"
    $("#btn_load_bank").click(function (event) {
        event.preventDefault();
        btn_get_banks();
    });

    //button's handler "Загрузить список валюты"
    $("#btn_load_cur").click(function (event) {
        event.preventDefault();
        btn_get_currency();
    });

    //button's handler "Добавить банк"
    $("#btn_add_bank").click(function (event) {
        $("#addbank").css("visibility", "visible");
        $("#addbank").attr("class", "modal fade");

        $("#form_add_bank").submit(function (event) {
            event.preventDefault();
            var $form = $(this),
                    bank_name = $form.find("input[name='bank_name']").val(),
                    bank_code = $form.find("input[name='bank_code']").val();

            if (bank_name != '' && bank_code != '') {
                var url = '/CreditBH/admin/save_bank';
                var bank_post = $.post(url, {bName: bank_name, bCode: bank_code});
                bank_post.done(function (data) {
                    var content = data;
                    if (content === 1) {
                        $("#status_bank").css("color", "blue");
                        $("#status_bank").text("Банк сохранен !");
                        btn_get_banks();
                    } else {
                        $("#status_bank").css("color", "red");
                        $("#status_bank").text("Ошибка !");
                    }
                });
            } else {
                $("#status_bank").css("color", "red");
                $("#status_bank").text("Ошибка ввода!");
            }
        });
    });

    //button's handler "Добавить валюту"
    $("#btn_add_curr").click(function (event) {
        $("#addcurr").css("visibility", "visible");
        $("#addcurr").attr("class", "modal fade");

        $("#form_add_curr").submit(function (event) {
            event.preventDefault();
            var $form = $(this),
                    curr_name = $form.find("input[name='curr_name']").val(),
                    curr_code = $form.find("input[name='curr_code']").val();
            if (curr_name != '' && curr_code != '') {
                var url = '/CreditBH/admin/save_currency';
                var bank_post = $.post(url, {currName: curr_name, currCode: curr_code});
                bank_post.done(function (data) {
                    var content = data;
                    if (content === 1) {
                        $("#status_bank").css("color", "blue");
                        $("#status_curr").text("Сохранено !");
                        btn_get_currency();
                    } else {
                        $("#status_curr").css("color", "red");
                        $("#status_curr").text("Ошибка !");
                    }
                });
            } else {
                $("#status_curr").css("color", "red");
                $("#status_curr").text("Ошибка ввода!");
            }
        });
    });

});


