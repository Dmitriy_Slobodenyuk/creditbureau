var dateFormat = /^((19|20)\d\d)-(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])$/;
var crd_edit_event;
var new_crd_event;
var new_crd_end_event;
var btn_click_client;

$(document).ready(function () {

    // валидация ввода даты на форме поиска клиента по ФИО
    $("#form_fio").find("input[name='bdate']").keyup(function () {
        var result = $(this).val().match(dateFormat);
        if (result === null && $(this).val() !== '') {
            $(this).css("border", "1px solid red");
        } else {
            $(this).css("border", "1px solid lightgrey");
            $("#error_date").text('');
        }
    });

    // валидация ввода даты при редактировании кредита
    crd_edit_event = function () {
        $("#input_date").keyup(function () {
            var input = $("#input_date").val();
            var result = input.match(dateFormat);
            if (result === null && input !== '') {
                $(this).css("border", "1px solid red");
            } else {
                $(this).css("border", "1px solid lightgrey");
                $("#date_credit_edit").text('');
            }
        });
    };

    // валидация ввода даты оформления кредита
    new_crd_event = function () {
        $("input[name='add_begin_date']").keyup(function () {
            var input = $("input[name='add_begin_date']").val();
            var result = input.match(dateFormat);
            if (result === null && input !== '') {
                $(this).css("border", "1px solid red");
            } else {
                $(this).css("border", "1px solid lightgrey");
                $("#start_date_edit").text('');
            }
        });
    };

    // валидация ввода даты завершения кредита
    new_crd_end_event = function () {
        $("input[name='add_end_date']").keyup(function () {
            var input = $("input[name='add_end_date']").val();
            var result = input.match(dateFormat);
            if (result === null && input !== '') {
                $(this).css("border", "1px solid red");
            } else {
                $(this).css("border", "1px solid lightgrey");
                $("#end_date_edit").text('');
            }
        });
    };

    // событие кнопки "все кредиты" у клиента
    btn_click_client = function () {
        $("#btn_all").click(function () {
            $("#allModal").css("visibility", "visible");
            $("#allModal").attr("class", "modal fade");
        });
    };

    // validation input date add new client form       
    $("#form_add_client").find("input[name='dBorn']").keyup(function () {
        var result = $(this).val().match(dateFormat);
        if (result === null && $(this).val() !== '') {
            $(this).css("border", "1px solid red");
        } else {
            $(this).css("border", "1px solid lightgrey");
            $("#error_date_add").text('');
        }
    });

    // find client by INN
    $("#form_inn").submit(function (event) {
        event.preventDefault();
        flag = true;
        var $form = $(this),
                term = $form.find("input[name='inn_inp']").val(),
                url = '/CreditBH/get_inn';
        var posting = $.post(url, {inn_inp: term});
        posting.done(function (data) {
            var content = data;
            $("#list_client").empty().append(content);
            btn_click_client();
            crd_edit_event();
            new_crd_event();
            new_crd_end_event();

        });
    });

    $("#form_fio").submit(function (event) {
        event.preventDefault();
        var $form = $(this),
                term1 = $form.find("input[name='fname']").val(),
                term2 = $form.find("input[name='sname']").val(),
                term3 = $form.find("input[name='lname']").val(),
                term4 = $form.find("input[name='bdate']").val(),
                url = '/CreditBH/get_fio';
        var date = term4.match(dateFormat);
        if (date === null) {
            $("#error_date").text('Недопустимый формат даты !');
        } else {
            $("#error_date").text('');
            var posting = $.post(url, {fname: term1, sname: term2, lname: term3, bdate: term4});
            posting.done(function (data) {
                var content = data;
                $("#list_client").empty().append(content);
                btn_click_client();
                crd_edit_event();
                new_crd_event();
                new_crd_end_event();
            });
        }
    });

    $("#form_passport").submit(function (event) {
        event.preventDefault();
        var $form = $(this),
                term1 = $form.find("input[name='s_pass']").val(),
                term2 = $form.find("input[name='n_pass']").val(),
                url = '/CreditBH/get_passport';
        var posting = $.post(url, {s_pass: term1, n_pass: term2});
        posting.done(function (data) {
            var content = data;
            $("#list_client").empty().append(content);
            btn_click_client();
            crd_edit_event();
            new_crd_event();
            new_crd_end_event();
        });
    });

    $("#form_add_client").submit(function (event) {
        event.preventDefault();
        var $form = $(this),
                term1 = $form.find("input[name='fName']").val(),
                term2 = $form.find("input[name='sName']").val(),
                term3 = $form.find("input[name='lName']").val(),
                term4 = $form.find("input[name='dBorn']").val(),
                term5 = $form.find("input[name='inn']").val(),
                term6 = $form.find("input[name='sPass']").val(),
                term7 = $form.find("input[name='nPass']").val(),
                url = '/CreditBH/add_new_client';

        var date = term4.match(dateFormat);
        if (date === null) {
            $("#error_date_add").text('Недопустимый формат даты !');
        } else {
            $("#error_date_add").text('');
            var posting = $.post(url,
                    {fName: term1, sName: term2, lName: term3, dBorn: term4,
                        inn: term5, sPass: term6, nPass: term7});
            posting.done(function (data) {
                var content = data;
                $("#list_client").empty().append(content);
            });
        }
    });
});

// Take the data from the form and send to the url to edit
function saveModifiedCredit() {
    var id = $("#id_sp").text();
    var id_cl = $("#id_cl").text();
    var body = $("input[name='ed_body']").val();
    var date = $("input[name='ed_date_end']").val();
    var delay = $("select[name='ed_delay'] option:selected").text();
    var url = '/CreditBH/save_md_credit';

    var result = date.match(dateFormat);
    if (result === null) {
        $("#date_credit_edit").text('Ошибка ввода !');
    } else {
        $("#date_credit_edit").text('');
        var posting = $.post(url,
                {id_credit: id, body_credit: body,
                    date_end: date, delay_credit: delay});
        posting.done(function (data) {
            var content = data;
            $("#oper").empty().append(content);
            var url_client = '/CreditBH/get_id';
            var posting_credit = $.get(url_client,
                    {id: id_cl});
            posting_credit.done(function (data) {
                $("#list_credit_div").empty().append(data);
            });
            var url = '/CreditBH/get_id_all';
            var posting_credit = $.get(url,
                    {id: id_cl});
            posting_credit.done(function (data) {
                $("#credit_all").empty().append(data);
            });
        });
    }
}

// Take the data from the form and send to the url to save
function saveNewCredit() {
    var id_cl = $("#id_cl").text();
    var bank = $("select[name='add_bank'] option:selected").text();
    var curr = $("select[name='add_curr'] option:selected").text();
    var body = $("input[name='add_bsumm']").val();
    var date_begin = $("input[name='add_begin_date']").val();
    var date_end = $("input[name='add_end_date']").val();
    var url = '/CreditBH/save_new_credit';

    var result_date_begin = date_begin.match(dateFormat);
    var result_date_end = date_end.match(dateFormat);
    if (result_date_begin === null) {
        $("#start_date_edit").text('Ошибка ввода !');
    } else if (result_date_end === null) {
        $("#end_date_edit").text('Ошибка ввода !');
    } else {
        $("#start_date_edit").text('');
        $("#end_date_edit").text('');
        var posting = $.post(url,
                {id_client: id_cl, bank_name: bank, curr_name: curr, body_begin: body,
                    date_begin: date_begin, date_end: date_end});
        posting.done(function (data) {
            var content = data;
            $("#oper").empty().append(content);
            var url_client = '/CreditBH/get_id';
            var posting_credit = $.get(url_client,
                    {id: id_cl});
            posting_credit.done(function (data) {
                $("#list_credit_div").empty().append(data);
            });
            var url = '/CreditBH/get_id_all';
            var posting_credit = $.get(url,
                    {id: id_cl});
            posting_credit.done(function (data) {
                $("#credit_all").empty().append(data);
            });
        });
    }
}

function editCredit(idCredit, bodyCredit, dateEnd, delay) {
    $("#date_credit_edit").text('');
    var url = '/CreditBH/credit_date_end';
    var posting_credit = $.get(url,
            {date: dateEnd});
    posting_credit.done(function (data) {
        $("#id_sp").text(idCredit);
        $("input[name='ed_body']").val(bodyCredit);
        $("input[name='ed_date_end']").val(data);
        $("select[name='ed_delay']").find("option:contains(" + delay + ")").attr("selected", "selected");
    });
}
