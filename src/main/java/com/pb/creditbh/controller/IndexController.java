package com.pb.creditbh.controller;

import com.pb.creditbh.controller.utils.ClientBuilder;
import com.pb.creditbh.controller.utils.CreditBuilder;
import com.pb.creditbh.controller.utils.CreditsSorter;
import com.pb.creditbh.model.entity.Client;
import com.pb.creditbh.controller.utils.FormDateFormatter;
import com.pb.creditbh.controller.utils.FormValidator;
import com.pb.creditbh.controller.utils.RestUrl;
import com.pb.creditbh.controller.utils.InterfaceMessages;
import com.pb.creditbh.model.entity.ClientWrapper;
import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.BankWrapper;
import com.pb.creditbh.model.entity.catalog.Currency;
import com.pb.creditbh.model.entity.catalog.CurrencyWrapper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Controller class that handles user requests the main the basic functionality
 * of the application
 *
 * @author Slobodenyuk Dmitriy
 */
@Controller
@RequestMapping("/")
public class IndexController {

    private static Logger loger = Logger.getLogger(IndexController.class.getName());

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    /**
     * When you start the program sends to the home page index.jsp
     * @param model
     * @return view
     */
    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        String helloView = "index";
        return helloView;
    }

    /**
     * When you click "Поиск клиентов" on the home page directs to a page with
     * work_page.jsp
     * @param model
     * @return view
     */
    @RequestMapping(value = "in", method = RequestMethod.GET)
    public String getWorkedPage(ModelMap model) {
        String view = "work_page";
        return view;
    }

    @RequestMapping(value = "admin_page", method = RequestMethod.GET)
    public String goToTestPage(ModelMap model) {
        String view = "admin_page";
        return view;
    }

    @RequestMapping(value = "credit_date_end", method = RequestMethod.GET)
    @ResponseBody
    public String getCreditDateEndToString(@RequestParam(value = "date", required = true) long date, ModelMap model) {
        Date d = new Date(date);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-dd-MM");
        String view = formatter.format(d);
        return view;
    }

    @RequestMapping(value = "get_id", method = RequestMethod.GET)
    public String getClientByIdWithActiveCredits(@RequestParam(value = "id", required = true) String id, Model model) {
        String view = "include/listCredits";
        List<Client> listClients = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        try {
            clientWrapper = restTemplate.getForObject(RestUrl.CLIENT_BY_ID.getUrl(), ClientWrapper.class, id);
            listClients = clientWrapper.getListClients();
            if (listClients != null || !listClients.isEmpty()) {
                List<Credit> crds = CreditsSorter.getCreditsActive(listClients.get(0).getListCredits());
                if (crds != null || !crds.isEmpty()) {
                    model.addAttribute("listActiveCredits", CreditsSorter.getCreditsActive(listClients.get(0).getListCredits()));
                }
            }
        } catch (RestClientException e) {
            loger.log(Level.SEVERE, "RestClientException !", e);
            return view;
        }
        return view;
    }

    @RequestMapping(value = "get_id_all", method = RequestMethod.GET)
    public String getClientByIdWithAllCredits(@RequestParam(value = "id", required = true) String id, Model model) {
        String view = "include/listAllCredits";
        List<Client> listClients = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        try {
            clientWrapper = restTemplate.getForObject(RestUrl.CLIENT_BY_ID.getUrl(), ClientWrapper.class, id);
            listClients = clientWrapper.getListClients();
            model.addAttribute("client", listClients.get(0));
        } catch (RestClientException e) {
            loger.log(Level.SEVERE, "RestClientException !", e);
            return view;
        }
        return view;
    }

    /**
     * Event processing the search form on the client's Inn, receiving an inn,  
     * and a request is sent to the service which resulted in returns to   Page
     * fragment jsp page which displays the customer or found   error
     */
    @RequestMapping(value = "get_inn", method = RequestMethod.POST)
    public String getClientByInn(@RequestParam(value = "inn_inp", required = true) String inn, Model model) {
        String view = null;
        List<Client> listClients = null;
        List<Bank> banks = null;
        List<Currency> currencies = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        if (FormValidator.isValidateInn(inn)) {
            try {
                clientWrapper = restTemplate.getForObject(RestUrl.INN_URL.getUrl(), ClientWrapper.class, inn);
                listClients = clientWrapper.getListClients();
                BankWrapper bankWrapper = new BankWrapper();
                CurrencyWrapper currencyWrapper = new CurrencyWrapper();
                bankWrapper = restTemplate.getForObject(RestUrl.ALL_BANKS.getUrl(), BankWrapper.class);
                currencyWrapper = restTemplate.getForObject(RestUrl.ALL_CURRENCIES.getUrl(), CurrencyWrapper.class);
                banks = bankWrapper.getListBanks();
                currencies = currencyWrapper.getList();
            } catch (RestClientException e) {
                view = "include/outError";
                loger.log(Level.SEVERE, "RestClientException !", e);
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                return view;
            }
            if (!listClients.isEmpty()) {
                view = "include/outClient";
                model.addAttribute(InterfaceMessages.LIST_CLIENTS_ATTR.toString(), listClients);
                if (!(listClients.get(0).getListCredits() == null)) {
                    model.addAttribute("listActiveCredits", CreditsSorter.getCreditsActive(listClients.get(0).getListCredits()));
                }
                model.addAttribute("listBanks", banks);
                model.addAttribute("listCurrency", currencies);
                return view;
            } else {
                view = "include/outError";
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.CLIENT_IS_NOT_AVAILABLE_INN.toString());
                return view;
            }
        } else {
            view = "include/outError";
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.INPUT_ERROR_FIELD_INN.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
            return view;
        }

    }

    /**
     * Processing events search form customer by name and date of birth,    
     * receiving an input, and sends the request to the service on the result
     * which is returned to the page fragment jsp page which displays customers
     * found or an error message
     */
    @RequestMapping(value = "get_fio", method = RequestMethod.POST)
    public String getClientByFio(@RequestParam(value = "fname", required = false) String fName,
            @RequestParam(value = "sname", required = false) String sName,
            @RequestParam(value = "lname", required = false) String lName,
            @RequestParam(value = "bdate", required = false) String bDate,
            Model model) {

        String view = null;
        List<Client> listClients = null;
        List<Bank> banks = null;
        List<Currency> currencies = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        if (FormValidator.isValidateFio(fName, sName, lName) && FormValidator.isValidateInputDate(bDate)) {
            try {
                if (lName == null || lName.equals("")) {
                    lName = "null";
                }
                clientWrapper = restTemplate.getForObject(RestUrl.FIO_AND_DATEBORN_URL.getUrl(), ClientWrapper.class, fName, sName, lName, FormDateFormatter.parseToLong(bDate));
                listClients = clientWrapper.getListClients();
                BankWrapper bankWrapper = new BankWrapper();
                CurrencyWrapper currencyWrapper = new CurrencyWrapper();
                bankWrapper = restTemplate.getForObject(RestUrl.ALL_BANKS.getUrl(), BankWrapper.class);
                currencyWrapper = restTemplate.getForObject(RestUrl.ALL_CURRENCIES.getUrl(), CurrencyWrapper.class);
                banks = bankWrapper.getListBanks();
                currencies = currencyWrapper.getList();
            } catch (RestClientException e) {
                view = "include/outError";
                loger.log(Level.SEVERE, "RestClientException !", e);
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                return view;
            }
            if (!listClients.isEmpty()) {
                view = "include/outClient";

                model.addAttribute(InterfaceMessages.LIST_CLIENTS_ATTR.toString(), listClients);
                if (!(listClients.get(0).getListCredits() == null)) {
                    model.addAttribute("listActiveCredits", CreditsSorter.getCreditsActive(listClients.get(0).getListCredits()));
                }
                model.addAttribute("listBanks", banks);
                model.addAttribute("listCurrency", currencies);
                return view;
            } else {
                view = "include/outError";
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.AT_REQUEST_NOTHING_FOUND.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                return view;
            }
        } else {
            view = "include/outError";
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.INPUT_ERROR_FIELD_FIO.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
            return view;
        }
    }

    /**
     * Processing events search form client passport data (series and      
     * number), receiving an input, and sends the request to the service on the
     * outcome of which is returned to the page fragment that jsp page brings
     * customers found or an error message
     */
    @RequestMapping(value = "get_passport", method = RequestMethod.POST)
    public String getClientByPassportData(@RequestParam(value = "s_pass", required = true) String sPass,
            @RequestParam(value = "n_pass", required = true) String nPass,
            Model model) {

        String view = null;
        List<Client> listClients = null;
        List<Bank> banks = null;
        List<Currency> currencies = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        if (FormValidator.isValidateDataPassport(sPass, nPass)) {
            try {
                clientWrapper = restTemplate.getForObject(RestUrl.PASSPORT_DATA_URL.getUrl(), ClientWrapper.class, sPass, nPass);
                listClients = clientWrapper.getListClients();
                BankWrapper bankWrapper = new BankWrapper();
                CurrencyWrapper currencyWrapper = new CurrencyWrapper();
                bankWrapper = restTemplate.getForObject(RestUrl.ALL_BANKS.getUrl(), BankWrapper.class);
                currencyWrapper = restTemplate.getForObject(RestUrl.ALL_CURRENCIES.getUrl(), CurrencyWrapper.class);
                banks = bankWrapper.getListBanks();
                currencies = currencyWrapper.getList();
            } catch (RestClientException e) {
                view = "include/outError";
                loger.log(Level.SEVERE, "RestClientException !", e);
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                return view;
            }
            if (!listClients.isEmpty()) {
                view = "include/outClient";
                model.addAttribute("listClient", listClients);
                if (!(listClients.get(0).getListCredits() == null)) {
                    model.addAttribute("listActiveCredits", CreditsSorter.getCreditsActive(listClients.get(0).getListCredits()));
                }
                model.addAttribute("listBanks", banks);
                model.addAttribute("listCurrency", currencies);
                return view;
            } else {
                view = "include/outError";
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.AT_REQUEST_NOTHING_FOUND.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                return view;
            }
        } else {
            view = "include/outError";
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.INPUT_ERROR_FIELD_DATA_PASSPORT.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
            return view;
        }

    }

    /**
     * Processing events form to add a new client, receiving an data, and sends
     * the request to the service which resulted in returns page fragment jsp
     * page which displays a message about the result this action
     */
    @RequestMapping(value = "add_new_client", method = RequestMethod.POST)
    public String addNewClient(@RequestParam(value = "fName", required = true) String fName,
            @RequestParam(value = "sName", required = true) String sName,
            @RequestParam(value = "lName", required = false) String lName,
            @RequestParam(value = "dBorn", required = true) String dBorn,
            @RequestParam(value = "inn", required = true) String inn,
            @RequestParam(value = "sPass", required = true) String sPass,
            @RequestParam(value = "nPass", required = true) String nPass,
            Model model) throws ParseException {
        String viewName = null;

        int status = 0;
        if (FormValidator.isValidateFormAddClient(fName, sName, lName, inn, sPass, nPass)) {
            Client newClient = new ClientBuilder().createClient(inn, fName, sName, lName, dBorn, sPass, nPass);
            try {
                status = restTemplate.postForObject(RestUrl.NEW_CLIENT_URL.getUrl(), newClient, Integer.class);
                if (status == 1) {
                    viewName = "include/success";
                    model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), "Клиент" + " " + newClient.getSecondName() + " " + newClient.getFirstName());
                    model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.ADD_SUCCESS.toString());
                } else if (status == 500) {
                    viewName = "include/outError";
                    model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.ERROR.toString());
                    model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                } else {
                    viewName = "include/outError";
                    model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.ERROR_DATA_ALREADY_EXISTS.toString());
                    model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
                }

            } catch (RestClientException e) {
                e.printStackTrace();
                viewName = "include/outError";
                loger.log(Level.SEVERE, "RestClientException !", e);
                model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
                model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
            }
        } else {
            viewName = "include/outError";
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.ERROR_INPUT_FORM_ADD_NEW_CLIENT.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
        }
        return viewName;
    }

    @RequestMapping(value = "save_md_credit", method = RequestMethod.POST)
    public String saveModifiedCredit(@RequestParam(value = "id_credit", required = true) String idCredit,
            @RequestParam(value = "body_credit", required = true) String bodyCredit,
            @RequestParam(value = "date_end", required = true) String dateEnd,
            @RequestParam(value = "delay_credit", required = true) String delayCredit, Model model) {

        String viewName = "include/status_operation";
        String response = "";

        if (idCredit.equals("") || bodyCredit.equals("") || dateEnd.equals("")) {
            response = "Пустые поля не допустимы !";
            model.addAttribute("message", response);
        } else {
            int res = 0;
            Credit credit = new CreditBuilder().createCredit(Long.valueOf(idCredit), bodyCredit,
                    FormDateFormatter.parseToLong(dateEnd), delayCredit);
            try {
                res = restTemplate.postForObject(RestUrl.MODIFIED_CREDIT.getUrl(), credit, Integer.class);

                if (res == 1) {
                    response = "Кредит успешно обновлен!";
                    model.addAttribute("message", response);
                } else {
                    response = "Ошибка обновления !";
                    model.addAttribute("message", response);
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                loger.log(Level.SEVERE, "RestClientException !", e);
                response = "Ошибка ! Не верно указан путь";
                model.addAttribute("message", response);
            }
        }
        return viewName;
    }

    @RequestMapping(value = "save_new_credit", method = RequestMethod.POST)
    public String saveNewCredit(@RequestParam(value = "id_client", required = true) String idClient,
            @RequestParam(value = "bank_name", required = true) String bankName,
            @RequestParam(value = "curr_name", required = true) String currName,
            @RequestParam(value = "body_begin", required = true) String bodyCredit,
            @RequestParam(value = "date_begin", required = true) String dateBegin,
            @RequestParam(value = "date_end", required = true) String dateEnd, Model model) {

        String viewName = "include/status_operation";
        String response = "";
        if (idClient.equals("") || bodyCredit.equals("")) {
            response = "Пустые поля не допустимы !";
            model.addAttribute("message", response);
        } else {
            int res = 0;
            Credit credit = new CreditBuilder().createCredit(Long.valueOf(idClient), bankName, currName,
                    bodyCredit, FormDateFormatter.parseToLong(dateBegin),
                    FormDateFormatter.parseToLong(dateEnd));

            try {
                res = restTemplate.postForObject(RestUrl.NEW_CREDIT.getUrl(), credit, Integer.class);

                if (res == 1) {
                    response = "Кредит успешно сохранен !";
                    model.addAttribute("message", response);
                } else {
                    response = "Ошибка сохранения !";
                    model.addAttribute("message", response);
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                loger.log(Level.SEVERE, "RestClientException !", e);
                response = "Ошибка ! Не верно указан путь";
                model.addAttribute("message", response);
            }
        }
        return viewName;
    }
}
