package com.pb.creditbh.controller.utils;

import com.pb.creditbh.model.entity.Client;

/**
 *
 * @author Slobodenyuk D.
 */
public class ClientBuilder {

    public ClientBuilder() {
    }

    public Client createClient(String inn, String firstName, String secondName, String lastName, String dateBorn, String serialPassport, String numberPassport, String image) {
        Client newClient = new Client();
        newClient.setFirstName(firstName);
        newClient.setSecondName(secondName);
        newClient.setLastName(lastName);
        newClient.setDateBorn(FormDateFormatter.parseToLong(dateBorn));
        newClient.setInn(inn);
        newClient.setSerialPassport(serialPassport);
        newClient.setNumberPassport(numberPassport);
        newClient.setImage(image);
        return newClient;
    }

    public Client createClient(String inn, String firstName, String secondName, String lastName, String dateBorn, String serialPassport, String numberPassport) {
        Client newClient = new Client();
        newClient.setFirstName(firstName);
        newClient.setSecondName(secondName);
        newClient.setLastName(lastName);
        newClient.setDateBorn(FormDateFormatter.parseToLong(dateBorn));
        newClient.setInn(inn);
        newClient.setSerialPassport(serialPassport);
        newClient.setNumberPassport(numberPassport);
        return newClient;
    }
}
