package com.pb.creditbh.controller.utils;

import java.util.regex.Pattern;

/**
 * @author Dmitriy Slobodenyuk
 */
public class FormValidator {

    public static boolean isValidateInn(String inn) {
        if (inn.matches("\\d{10}")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidateInputDate(String date) {
        String regex = "^((19|20)\\d\\d)-(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])$";
        return Pattern.matches(regex, date);
    }

    public static boolean isValidateDataPassport(String serial, String number) {
        if (serial.matches("[А-Я][А-Я]") && number.matches("\\d{6}")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidateFio(String firstName, String secondName, String lastName) {
        if (lastName.equals("")) {
            if (firstName.matches("[А-ЯA-Z][а-яa-z]{1,25}") && secondName.matches("[А-ЯA-Z][а-яa-z]{1,25}")) {
                return true;
            } else {
                return false;
            }
        } else {
            if (firstName.matches("[А-ЯA-Z][а-яa-z]{1,25}") && secondName.matches("[А-ЯA-Z][а-яa-z]{1,25}") && lastName.matches("[А-ЯA-Z][а-яa-z]{1,25}")) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static boolean isValidateFormAddClient(String firstName, String secondName, String lastName,
            String inn, String serialPassport, String numberPassport) {
        if (isValidateFio(firstName, secondName, lastName)
                && isValidateInn(inn) && isValidateDataPassport(serialPassport, numberPassport)) {
            return true;
        } else {
            return false;
        }
    }
}
