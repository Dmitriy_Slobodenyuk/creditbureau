package com.pb.creditbh.controller.utils;

/**
 * Class provides string constants that store url path to Web services
 */
public enum RestUrl {

    INN_URL("http://localhost:8084/CreditBH/client/client_by_inn/{inn}"),
    FIO_AND_DATEBORN_URL("http://localhost:8084/CreditBH/client/client_by_fio/{fname}/{sname}/{lname}/{bdate}"),
    PASSPORT_DATA_URL("http://localhost:8084/CreditBH/client/client_by_passport/{ser}/{num}"),
    NEW_CLIENT_URL("http://localhost:8084/CreditBH/client/new_client"),
    ALL_BANKS("http://localhost:8084/CreditBH/client/all_banks"),
    ALL_CURRENCIES("http://localhost:8084/CreditBH/client/all_currencies"),
    MODIFIED_CREDIT("http://localhost:8084/CreditBH/client/modified_credit"),
    NEW_CREDIT("http://localhost:8084/CreditBH/client/new_credit"),
    CLIENT_BY_ID("http://localhost:8084/CreditBH/client/client_by_id/{id}"),
    SAVE_BANK("http://localhost:8084/CreditBH/client/add_bank_service"),
    SAVE_CURRENCY("http://localhost:8084/CreditBH/client/add_currency_service")
    ;

    private final String url;

    RestUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
