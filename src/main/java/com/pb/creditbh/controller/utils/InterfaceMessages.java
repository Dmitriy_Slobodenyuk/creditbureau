package com.pb.creditbh.controller.utils;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public enum InterfaceMessages {

    MESSAGE_1("message1"),
    MESSAGE_2("message2"),
    PATH_IS_NOT_TRUE_OR_ERROR_SERVICE("Не верно указан путь или ошибка сервиса !"),
    TRY_AGAIN("Попробуйте ещё раз !"),
    CLIENT_IS_NOT_AVAILABLE_INN("Клиент с заданным ИНН отсуствует !"),
    INPUT_ERROR_FIELD_INN("Ошибка ввода ! Поле не может быть пустым и должно содержать 10 цифр"),
    AT_REQUEST_NOTHING_FOUND("По вашему запросу ничего не найдено !"),
    INPUT_ERROR_FIELD_FIO("Поля Имя и Фамилия не могут быть пустыми и должны начинаться с заглавной буквы !"),
    INPUT_ERROR_FIELD_DATA_PASSPORT("Ошибка ввода : Пустые поля не допустимы ,Серия (две заглавные буквы), Номер(6 - цифр) !"),
    ADD_SUCCESS("Успешно добавлен"),
    ERROR("Ошибка (SQLException) !"),
    ERROR_IF_EXISTS("Ошибка клиент с такими данными уже существует !"),
    LIST_CLIENTS_ATTR("listClient"),
    ERROR_DATA_ALREADY_EXISTS("Ошибка ! Клиент с такими данными уже существует ! Проверьте ИНН или паспортные данные"),
    ERROR_INPUT_FORM_ADD_NEW_CLIENT("Ошибка ввода : поля не должны быть пустыми , ИНН - 10 цифр , серия паспорта - две заглавные буквы, номер паспорта - 6 цифр !")
    ;
    private final String message;

    InterfaceMessages(String message) {
        this.message = message;
    }

    public String toString() {
        return message;
    }
}
