package com.pb.creditbh.controller.utils;

import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import java.math.BigDecimal;

/**
 *
 * @author Slobodenyuk D.
 */
public class CreditBuilder {

    public CreditBuilder() {
    }

    public Credit createCredit(long idCredit, String bodyCredit, long dateEnd, String delayCredit) {
        Credit credit = new Credit();
        credit.setIdCredit(idCredit);
        credit.setCreditCurrBody(BigDecimal.valueOf(Double.valueOf(bodyCredit)));
        credit.setCreditEnd(dateEnd);
        credit.setOutDelay(delayCredit);
        return credit;
    }

    public Credit createCredit(long idClient, String bankName, String currencyName,
            String bodyCredit, long dateStart, long dateEnd) {
        Credit credit = new Credit();
        credit.setClient(idClient);
        Bank bank = new Bank();
        bank.setBankName(bankName);
        credit.setBank(bank);
        Currency curr = new Currency();
        curr.setCurrName(currencyName);
        credit.setCurrency(curr);
        credit.setBaseAmount(BigDecimal.valueOf(Double.valueOf(bodyCredit)));
        credit.setCreditDateIssue(dateStart);
        credit.setCreditEnd(dateEnd);
        return credit;
    }
}
