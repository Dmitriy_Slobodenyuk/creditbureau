package com.pb.creditbh.controller.utils;

import com.pb.creditbh.model.entity.Credit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Get Active Credits
 *
 * @author Slobodenyuk D.
 */
public class CreditsSorter {

    public static List<Credit> getCreditsActive(List<Credit> listCredits) {
        List<Credit> listActiveCredits = new ArrayList<Credit>();
        listCredits.forEach((credit) -> {
            if (credit.getCreditEnd() > new Date().getTime()) {
                listActiveCredits.add(credit);
            }
        });
        return listActiveCredits;
    }
}
