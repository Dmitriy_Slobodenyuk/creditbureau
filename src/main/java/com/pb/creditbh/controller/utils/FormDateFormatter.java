package com.pb.creditbh.controller.utils;

import com.pb.creditbh.controller.AdminController;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class FormDateFormatter {

    private static Logger loger = Logger.getLogger(FormDateFormatter.class.getName());    
    private static String dateFormat = "yyyy-dd-MM";

    public FormDateFormatter() {

    }

    public static Date parse(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Date date = null;
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            loger.log(Level.SEVERE, "ParseException !", e);
        }
        return date;
    }

    public static long parseToLong(String dateString) {
        long time = 0;
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Date date = null;
        try {
            date = formatter.parse(dateString);
            time = date.getTime();
        } catch (ParseException e) {
            loger.log(Level.SEVERE, "ParseException !", e);
        }
        return time;
    }
}
