package com.pb.creditbh.controller;

import com.pb.creditbh.model.entity.Client;
import com.pb.creditbh.model.entity.ClientWrapper;
import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.model.entity.CreditWrapper;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.BankWrapper;
import com.pb.creditbh.model.entity.catalog.Currency;
import com.pb.creditbh.model.entity.catalog.CurrencyWrapper;
import com.pb.creditbh.service.CatalogSpringComponent;
import com.pb.creditbh.service.interfaces.IClientComponent;
import com.pb.creditbh.service.interfaces.ICreditComponent;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * REST API provides: Search customers by : inn, name + date of birth, series
 * and passport number ,to obtain a list of credits in a specific client add /
 * update the information on the loan with the addition of customer beating
 * doubles (search criteria and try to find already instigated a client) if for
 *   API request does not find anything, then null is returned
 *
 * @author Dmitriy Slobodenyuk
 */
@Controller
@RequestMapping("client")
public class ClientController {

    private static Logger loger = Logger.getLogger(ClientController.class.getName());

    @Autowired
    @Qualifier("clientSpringComponent")
    private IClientComponent clientComponent;

    @Autowired
    @Qualifier("creditSpringComponent")
    private ICreditComponent creditComponent;

    @Autowired
    private CatalogSpringComponent catalogSpringComponent;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        String helloView = "work_page";
        return helloView;
    }

    @RequestMapping(value = "/client_by_id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ClientWrapper getClientById(@PathVariable String id, Model model) {
        List<Client> list = clientComponent.getById(Long.valueOf(id));
        ClientWrapper clientWrapper = new ClientWrapper(list);
        return clientWrapper;
    }

    @RequestMapping(value = "/client_by_inn/{inn}", method = RequestMethod.GET)
    public @ResponseBody
    ClientWrapper getClientByInn(@PathVariable String inn, Model model) {

        List<Client> list = clientComponent.getByInn(inn);
        ClientWrapper clientWrapper = new ClientWrapper(list);
        return clientWrapper;
    }

    @RequestMapping(value = "/client_by_passport/{ser}/{num}", method = RequestMethod.GET)
    public @ResponseBody
    ClientWrapper getClientByPassportData(@PathVariable("ser") String serial,
            @PathVariable("num") String number,
            Model model) {
        List<Client> list = clientComponent.getByDataPassport(serial, number);
        ClientWrapper clientWrapper = new ClientWrapper(list);
        return clientWrapper;
    }

    @RequestMapping(value = "/client_by_fio/{fname}/{sname}/{lname}/{bdate}", method = RequestMethod.GET)
    public @ResponseBody
    ClientWrapper getClientByFIO(@PathVariable("fname") String fName,
            @PathVariable("sname") String sName,
            @PathVariable("lname") String lName,
            @PathVariable("bdate") long bDate,
            Model model) {
        List<Client> list = clientComponent.getByFioAndDateBorn(fName, sName, lName, bDate);
        ClientWrapper clientWrapper = new ClientWrapper(list);
        return clientWrapper;
    }

    /**
     * @return 1 - if new client added
     * @return 0 - if a customer already exists
     * @return 500 - SQLException
     */
    @RequestMapping(value = "/new_client", method = RequestMethod.POST)
    public @ResponseBody
    int addClient(@RequestBody Client newClient,
            Model model) {
        int response = 0;

        try {
            response = clientComponent.addClient(newClient);
        } catch (InvalidResultSetAccessException e) {
            response = 500;
            e.printStackTrace();
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            e.printStackTrace();
            response = 500;
            loger.log(Level.SEVERE, "DataAccessException !", e);
        } catch (SQLException e) {
            e.printStackTrace();
            response = 500;
            loger.log(Level.SEVERE, "SQLException !", e);
        }

        return response;
    }

    @RequestMapping(value = "/modified_credit", method = RequestMethod.POST)
    public @ResponseBody
    int saveModifiedCredit(@RequestBody Credit credit,
            Model model) {
        int response = 0;

        try {
            response = creditComponent.updateCreditInfo(credit);
        } catch (InvalidResultSetAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "DataAccessException !", e);
        }
        return response;
    }

    @RequestMapping(value = "/new_credit", method = RequestMethod.POST)
    public @ResponseBody
    int saveNewCredit(@RequestBody Credit credit,
            Model model) {
        int response = 0;

        try {
            response = creditComponent.addNewCredit(credit);
        } catch (InvalidResultSetAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "DataAccessException !", e);
        }
        return response;
    }

    @RequestMapping(value = "/credit_by_id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    CreditWrapper getCreditById(@PathVariable("id") long id, Model model) {
        List<Credit> list = null;
        CreditWrapper wrapper = null;
        try {
            list = creditComponent.getById(id);
            wrapper = new CreditWrapper(list);
        } catch (InvalidResultSetAccessException e) {
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            loger.log(Level.SEVERE, "DataAccessException !", e);
        }
        return wrapper;
    }

    @RequestMapping(value = "/credit_by_id_credit/{id}", method = RequestMethod.GET)
    public @ResponseBody
    CreditWrapper getCreditByIdCredit(@PathVariable("id") long id, Model model) {
        List<Credit> list = null;
        CreditWrapper wrapper = null;
        try {
            list = creditComponent.getByIdCredit(id);
            wrapper = new CreditWrapper(list);
        } catch (InvalidResultSetAccessException e) {
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            loger.log(Level.SEVERE, "DataAccessException !", e);
        }
        return wrapper;
    }

    @RequestMapping(value = "/add_bank_service", method = RequestMethod.POST)
    public @ResponseBody
    int addBank(@RequestBody Bank bank, Model model) {
        int response = 0;
        try {
            response = catalogSpringComponent.addBank(bank);
        } catch (InvalidResultSetAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        }
        return response;
    }

    @RequestMapping(value = "/add_currency_service", method = RequestMethod.POST)
    public @ResponseBody
    int addCurrency(@RequestBody Currency curr, Model model) {
        int response = 0;
        try {
            response = catalogSpringComponent.addCurrency(curr);
        } catch (InvalidResultSetAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "InvalidResultSetAccessException !", e);
        } catch (DataAccessException e) {
            response = 500;
            loger.log(Level.SEVERE, "DataAccessException !", e);
        }
        return response;
    }

    @RequestMapping(value = "/all_banks", method = RequestMethod.GET)
    public @ResponseBody
    BankWrapper getAllBanks(Model model) {
        List<Bank> list = catalogSpringComponent.getBanks();
        BankWrapper bankWrapper = new BankWrapper(list);
        return bankWrapper;
    }

    @RequestMapping(value = "/all_currencies", method = RequestMethod.GET)
    public @ResponseBody
    CurrencyWrapper getAllCurrencies(Model model) {
        List<Currency> list = catalogSpringComponent.getCurrencies();
        CurrencyWrapper currencyWrapper = new CurrencyWrapper(list);
        return currencyWrapper;
    }

}
