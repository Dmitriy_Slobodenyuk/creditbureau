package com.pb.creditbh.controller;

import com.pb.creditbh.controller.utils.InterfaceMessages;
import com.pb.creditbh.controller.utils.RestUrl;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.BankWrapper;
import com.pb.creditbh.model.entity.catalog.Currency;
import com.pb.creditbh.model.entity.catalog.CurrencyWrapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping("admin")
public class AdminController {

    private static Logger loger = Logger.getLogger(AdminController.class.getName());

    /**
     * Processing request to save the new bank
     *
     * @param bankName
     * @param bankCode
     * @param model
     * @return status operation
     */
    @RequestMapping(value = "/save_bank", method = RequestMethod.POST)
    public @ResponseBody
    int saveNewBank(@RequestParam(value = "bName", required = true) String bankName,
            @RequestParam(value = "bCode", required = true) String bankCode, Model model) {
        int response = 0;
        if (bankCode.length() <= 6) {
            Bank newBank = new Bank(bankCode, bankName);
            response = new RestTemplate().postForObject(RestUrl.SAVE_BANK.getUrl(), newBank, Integer.class);
            return response;
        } else {
            return response;
        }
    }

    /**
     * Processing request to save the new currency
     *
     * @param currName
     * @param currCode
     * @param model
     * @return status operation
     */
    @RequestMapping(value = "/save_currency", method = RequestMethod.POST)
    public @ResponseBody
    int saveNewCurrency(@RequestParam(value = "currName", required = true) String currName,
            @RequestParam(value = "currCode", required = true) String currCode, Model model) {
        int response = 0;
        if (currCode.length() <= 3) {
            Currency currency = new Currency(currCode, currName);
            response = new RestTemplate().postForObject(RestUrl.SAVE_CURRENCY.getUrl(), currency, Integer.class);
            return response;
        } else {
            return response;
        }

    }

    /**
     * Processing a request for get list of banks
     *
     * @param model
     * @return List banks
     */
    @RequestMapping(value = "/getListBanks", method = RequestMethod.GET)
    public @ResponseBody
    List<Bank> getListBanks(Model model) {
        BankWrapper bankWrapper = null;
        List<Bank> banks = null;
        try {
            bankWrapper = new RestTemplate().getForObject(RestUrl.ALL_BANKS.getUrl(), BankWrapper.class);
            banks = bankWrapper.getListBanks();
        } catch (RestClientException e) {
            loger.log(Level.SEVERE, "RestClientException !", e);
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
        }
        return banks;
    }

    /**
     * Processing a request for get list of currencies
     *
     * @param model
     * @return List currencies
     */
    @RequestMapping(value = "/getCurrency", method = RequestMethod.GET)
    public @ResponseBody
    List<Currency> getCurrency(Model model) {
        CurrencyWrapper currencyWrapper = null;
        List<Currency> currencies = null;
        try {
            currencyWrapper = new RestTemplate().getForObject(RestUrl.ALL_CURRENCIES.getUrl(), CurrencyWrapper.class);
            currencies = currencyWrapper.getList();
        } catch (RestClientException e) {
            loger.log(Level.SEVERE, "RestClientException !", e);
            model.addAttribute(InterfaceMessages.MESSAGE_1.toString(), InterfaceMessages.PATH_IS_NOT_TRUE_OR_ERROR_SERVICE.toString());
            model.addAttribute(InterfaceMessages.MESSAGE_2.toString(), InterfaceMessages.TRY_AGAIN.toString());
        }
        return currencies;
    }
}
