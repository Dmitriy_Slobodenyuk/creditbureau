package com.pb.creditbh.model.entity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "credits")
public class CreditWrapper {

    List<Credit> list = new ArrayList<Credit>();

    public CreditWrapper() {
    }

    public CreditWrapper(List<Credit> list) {
        this.list = list;
    }

    @XmlElement(name = "credit")
    public List<Credit> getList() {
        return list;
    }

    public void setList(List<Credit> list) {
        this.list = list;
    }
}
