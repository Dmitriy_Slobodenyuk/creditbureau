package com.pb.creditbh.model.entity.catalog;

import com.pb.creditbh.model.entity.catalog.Bank;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "banks")
public class BankWrapper {

    private List<Bank> listBanks = new ArrayList<Bank>();

    public BankWrapper() {
    }

    public BankWrapper(List<Bank> list) {
        this.listBanks = list;
    }

    @XmlElement(name = "bank")
    public List<Bank> getListBanks() {
        return listBanks;
    }

    public void setListBanks(List<Bank> listBanks) {
        this.listBanks = listBanks;
    }
    
}
