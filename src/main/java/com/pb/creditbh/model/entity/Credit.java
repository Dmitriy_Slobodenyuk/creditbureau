package com.pb.creditbh.model.entity;

import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity Credit
 *
 * @author Dmitriy Slobodenyuk
 */
@XmlRootElement()
public class Credit implements Serializable {

    private long idCredit;
    private Currency currency;
    private Bank bank;
    private long client;
    private BigDecimal baseAmount;
    private long creditDateIssue;
    private BigDecimal creditCurrBody;
    private long creditEnd;
    private String outDelay;
    private String dateFormat = "yyyy-dd-MM";

    public Credit() {

    }

    public Credit(int idCredit, Currency idCurrency, Bank idBank, long idClient,
            BigDecimal baseAmount, long creditDateIssue, BigDecimal creditCurrBody, long creditEnd, String outDelay) {
        this.idCredit = idCredit;
        this.currency = idCurrency;
        this.bank = idBank;
        this.client = idClient;
        this.baseAmount = baseAmount;
        this.creditDateIssue = creditDateIssue;
        this.creditCurrBody = creditCurrBody;
        this.creditEnd = creditEnd;
        this.outDelay = outDelay;
    }

    public long getIdCredit() {
        return idCredit;
    }

    public void setIdCredit(long idCredit) {
        this.idCredit = idCredit;
    }

    @XmlElement(name = "currency")
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @XmlElement(name = "bank")
    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public long getClient() {
        return client;
    }

    public void setClient(long client) {
        this.client = client;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public long getCreditDateIssue() {
        return creditDateIssue;
    }

    public void setCreditDateIssue(long creditDateIssue) {
        this.creditDateIssue = creditDateIssue;
    }

    public BigDecimal getCreditCurrBody() {
        return creditCurrBody;
    }

    public void setCreditCurrBody(BigDecimal creditCurrBody) {
        this.creditCurrBody = creditCurrBody;
    }

    public long getCreditEnd() {
        return creditEnd;
    }

    public void setCreditEnd(long creditEnd) {
        this.creditEnd = creditEnd;
    }

    public String getOutDelay() {
        return outDelay;
    }

    public void setOutDelay(String outDelay) {
        this.outDelay = outDelay;
    }

    public String getDateIssueToString() {
        String date = "";
        Date d = new Date(creditDateIssue);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        date = formatter.format(d);
        return date;
    }

    public String getDateEndToString() {
        String date = "";
        Date d = new Date(creditEnd);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        date = formatter.format(d);
        return date;
    }

    @Override
    public String toString() {
        return "Банк = " + bank.getBankName() + ", оформлен = " + creditDateIssue;
    }

}
