package com.pb.creditbh.model.entity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Slobodenyuk D.
 */
@XmlRootElement(name = "clients")
public class ClientWrapper {

    private List<Client> listClients = new ArrayList<Client>();

    public ClientWrapper() {
    }

    public ClientWrapper(List<Client> listClients) {
        this.listClients = listClients;
    }

    @XmlElement(name = "client")
    public List<Client> getListClients() {
        return listClients;
    }

    public void setListClients(List<Client> listClients) {
        this.listClients = listClients;
    }
    
}
