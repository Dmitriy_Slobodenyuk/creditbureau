package com.pb.creditbh.model.dao.intefaces;

import com.pb.creditbh.model.entity.Client;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IClientDao {

    public List<Client> findByINN(String inn);
    
    public List<Client> findById(long id);

    public List<Client> findByFIOandDateBorn(String firstName, String secondName,
            String lastName, long dateBorn);

    public List<Client> findByDataPassport(String sPass, String nPass);

    public int save(Client client);
}
