package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("catalogDao")
public class CatalogDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Bank> getBank(String bankCode) {
        String query = "SELECT BNK_CODE,BNK_NAME FROM Bank_Dict WHERE BNK_CODE=?";
        return jdbcTemplate.query(query, new Object[]{bankCode}, new BankCreatorForSpring());
    }

    public List<Bank> getBankByName(String bankName) {
        String query = "SELECT BNK_CODE,BNK_NAME FROM Bank_Dict WHERE BNK_NAME=?";
        return jdbcTemplate.query(query, new Object[]{bankName}, new BankCreatorForSpring());
    }

    @Transactional
    public int addNewBank(Bank bank) {
        int result = 0;
        String query = "INSERT INTO Bank_Dict VALUES(?, ?)";
        result = jdbcTemplate.update(query, new Object[]{bank.getBankCode(), bank.getBankName()});
        return result;
    }

    public List<Bank> getAllBanks() {
        String query = "SELECT BNK_CODE,BNK_NAME FROM Bank_Dict";
        return jdbcTemplate.query(query, new Object[]{}, new BankCreatorForSpring());
    }

    public List<Currency> getAllCurrency() {
        String query = "SELECT CSH_ID, CSH_NAM FROM Cash_Dict";
        return jdbcTemplate.query(query, new Object[]{}, new CurrencyCreatorForSpring());
    }

    public List<Currency> getCurrency(String currencyCode) {
        String query = "SELECT CSH_ID, CSH_NAM FROM Cash_Dict WHERE CSH_ID=?";
        return jdbcTemplate.query(query, new Object[]{currencyCode}, new CurrencyCreatorForSpring());
    }

    public List<Currency> getCurrencyByName(String currencyName) {
        String query = "SELECT CSH_ID, CSH_NAM FROM Cash_Dict WHERE CSH_NAM=?";
        return jdbcTemplate.query(query, new Object[]{currencyName}, new CurrencyCreatorForSpring());
    }

    @Transactional
    public int addNewCurrency(Currency currency) {
        int result = 0;
        String query = "INSERT INTO Cash_Dict VALUES(?, ?)";
        result = jdbcTemplate.update(query, new Object[]{currency.getCurrCode(), currency.getCurrName()});
        return result;
    }

}

class BankCreatorForSpring implements RowMapper<Bank> {

    @Override
    public Bank mapRow(ResultSet rs, int rowNum) throws SQLException {
        Bank bank = new Bank();
        bank.setBankCode(rs.getString("BNK_CODE"));
        bank.setBankName(rs.getString("BNK_NAME"));
        return bank;
    }
}

class CurrencyCreatorForSpring implements RowMapper<Currency> {

    @Override
    public Currency mapRow(ResultSet rs, int rowNum) throws SQLException {
        Currency currency = new Currency();
        currency.setCurrCode(rs.getString("CSH_ID"));
        currency.setCurrName(rs.getString("CSH_NAM"));
        return currency;
    }
}
