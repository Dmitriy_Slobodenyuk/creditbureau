package com.pb.creditbh.model.dao.intefaces;

import com.pb.creditbh.model.entity.Credit;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface ICreditDao {

    public List<Credit> getCredits(long idClient);

    public int save(Credit credit);

    public int update(Credit credit);
    
    public List<Credit> getCreditById(long credit);
}
