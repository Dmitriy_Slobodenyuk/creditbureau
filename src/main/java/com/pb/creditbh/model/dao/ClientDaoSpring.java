package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.dao.intefaces.IClientDao;
import com.pb.creditbh.model.entity.Client;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
@Repository("clientDaoSpring")
public class ClientDaoSpring implements IClientDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcCall jdbcCall;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Client> findByINN(String inn) {
        String query = "SELECT CLN_ID, CLN_INN, CLN_NAM1, CLN_NAM2, CLN_NAM3, "
                + "CLN_BD, CLN_PS, CLN_PN, CLN_IMG FROM client WHERE CLN_INN=?";
        return jdbcTemplate.query(query, new Object[]{inn}, new ClientCreatorForSpring());
    }

    @Override
    public List<Client> findById(long id) {
        String query = "SELECT CLN_ID, CLN_INN, CLN_NAM1, CLN_NAM2, CLN_NAM3, "
                + "CLN_BD, CLN_PS, CLN_PN, CLN_IMG FROM client WHERE CLN_ID=?";
        return jdbcTemplate.query(query, new Object[]{id}, new ClientCreatorForSpring());
    }

    @Override
    public List<Client> findByFIOandDateBorn(String firstName, String secondName,
            String lastName, long dateBorn) {

        if (lastName.equals("null")) {
            String query = "SELECT CLN_ID, CLN_INN, CLN_NAM1, "
                    + "CLN_NAM2, CLN_NAM3, CLN_BD, CLN_PS, "
                    + "CLN_PN, CLN_IMG FROM client WHERE CLN_NAM1=? "
                    + "and CLN_NAM2=? and CLN_NAM3=? and CLN_BD=?";
            return jdbcTemplate.query(query, new Object[]{firstName, secondName, "", dateBorn}, new ClientCreatorForSpring());
        } else {
            String query = "SELECT CLN_ID, CLN_INN, CLN_NAM1, CLN_NAM2, "
                    + "CLN_NAM3, CLN_BD, CLN_PS, CLN_PN, "
                    + "CLN_IMG FROM client WHERE CLN_NAM1=? "
                    + "and CLN_NAM2=? and CLN_NAM3=? and CLN_BD=?";
            return jdbcTemplate.query(query, new Object[]{firstName, secondName, lastName, dateBorn}, new ClientCreatorForSpring());
        }
    }

    @Override
    public List<Client> findByDataPassport(String sPass, String nPass) {
        String query = "SELECT CLN_ID, CLN_INN, CLN_NAM1, CLN_NAM2, "
                + "CLN_NAM3, CLN_BD, CLN_PS, CLN_PN, CLN_IMG "
                + "FROM client WHERE CLN_PS=? AND CLN_PN=?";
        return jdbcTemplate.query(query, new Object[]{sPass, nPass}, new ClientCreatorForSpring());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int save(Client client) {
        String query = "INSERT INTO client (CLN_INN, CLN_NAM1, CLN_NAM2, "
                + "CLN_NAM3, CLN_BD, CLN_PS, CLN_PN, CLN_IMG"
                + ") VALUES(:inn, :fName, :sName,"
                + ":lName, :dateBorn, :serPass, :numPass, :img)";
        int result = 0;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("inn", client.getInn());
        params.addValue("fName", client.getFirstName());
        params.addValue("sName", client.getSecondName());
        params.addValue("lName", client.getLastName());
        params.addValue("dateBorn", client.getDateBorn());
        params.addValue("serPass", client.getSerialPassport());
        params.addValue("numPass", client.getNumberPassport());
        params.addValue("img", client.getImage());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        result = namedParameterJdbcTemplate.update(query, params, keyHolder);

        return result;
    }

    private long getIdentityKey() {
        long id = 0;
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("addID");
        Map<String, Object> inParamMap = new HashMap<String, Object>();
        inParamMap.put("table_Name", "client");
        inParamMap.put("NXTID", id);
        SqlParameterSource in = new MapSqlParameterSource(inParamMap);

        return id = simpleJdbcCall.executeObject(Integer.class, in);
    }
}

class ClientCreatorForSpring implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        Client client = new Client();
        client.setIdClient(rs.getLong("CLN_ID"));
        client.setInn(rs.getString("CLN_INN"));
        client.setFirstName(rs.getString("CLN_NAM1"));
        client.setSecondName(rs.getString("CLN_NAM2"));
        client.setLastName(rs.getString("CLN_NAM3"));
        client.setDateBorn(rs.getLong("CLN_BD"));
        client.setSerialPassport(rs.getString("CLN_PS"));
        client.setNumberPassport(rs.getString("CLN_PN"));
        client.setImage(rs.getString("CLN_IMG"));
        return client;
    }
}
