package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.dao.intefaces.ICreditDao;
import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
@Repository("creditDaoSpring")
public class CreditDaoSpring implements ICreditDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcCall jdbcCall;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private CatalogDao catalogDao;

    @Override
    public List<Credit> getCredits(long idClient) {
        String query = "SELECT c1.CRD_ID, c1.CLN_ID,"
                + "c1.BNK_CODE, c1.CRD_BSUM,c1.CSH_ID,c1.CRD_BDATE,"
                + "c1.CRD_CBODY,c1.CRD_ENDDATE,c1.CRD_OUT "
                + "FROM credit as c1, client as c2 WHERE c1.CLN_ID=? AND c1.CLN_ID=c2.CLN_ID";
        return jdbcTemplate.query(query, new Object[]{idClient}, new CreditCreatorForSpring());
    }

    @Override
    public List<Credit> getCreditById(long idCredit) {
        String query = "SELECT CRD_ID, CLN_ID, BNK_CODE, CRD_BSUM, CSH_ID, "
                + "CRD_BDATE, CRD_CBODY, CRD_ENDDATE, CRD_OUT FROM credit WHERE CRD_ID=?";
        return jdbcTemplate.query(query, new Object[]{idCredit}, new CreditCreatorForSpring());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int save(Credit credit) {
        String query = "INSERT INTO credit (CLN_ID, BNK_CODE, CRD_BSUM, CSH_ID, "
                + "CRD_BDATE, CRD_CBODY, CRD_ENDDATE, CRD_OUT) VALUES(:idClient, :bankCode,"
                + " :baseAmount, :currCode, :dateStartCredit, "
                + ":currBody, :dateEndCredit, :outDelay)";
        int result = 0;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idClient", credit.getClient());
        params.addValue("bankCode", credit.getBank().getBankCode());
        params.addValue("baseAmount", credit.getBaseAmount());
        params.addValue("currCode", credit.getCurrency().getCurrCode());
        params.addValue("dateStartCredit", credit.getCreditDateIssue());
        params.addValue("currBody", credit.getCreditCurrBody());
        params.addValue("dateEndCredit", credit.getCreditEnd());
        params.addValue("outDelay", credit.getOutDelay());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        result = namedParameterJdbcTemplate.update(query, params, keyHolder);

        return result;
    }

    @Override
    @Transactional
    public int update(Credit credit) {
        String query = "UPDATE credit SET CRD_CBODY=?, CRD_ENDDATE=?, CRD_OUT=? WHERE CRD_ID=?";
        int result = 0;
        String delay = "";
        if (credit.getOutDelay().equals("Да")) {
            delay = "Y";
        } else {
            delay = "N";
        }
        result = jdbcTemplate.update(query, new Object[]{credit.getCreditCurrBody(), credit.getCreditEnd(),
            delay, credit.getIdCredit()});

        return result;
    }

    private long getIdentityKey() {
        long id = 0;
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("addID");
        Map<String, Object> inParamMap = new HashMap<String, Object>();
        inParamMap.put("table_Name", "credit");
        inParamMap.put("NXTID", id);
        SqlParameterSource in = new MapSqlParameterSource(inParamMap);
        return id = simpleJdbcCall.executeObject(Integer.class, in);
    }

    class CreditCreatorForSpring implements RowMapper<Credit> {

        @Override
        public Credit mapRow(ResultSet rs, int rowNum) throws SQLException {
            Credit credit = new Credit();
            credit.setIdCredit(rs.getLong("CRD_ID"));
            credit.setClient(rs.getLong("CLN_ID"));
            Bank bank = catalogDao.getBank(rs.getString("BNK_CODE")).get(0);
            credit.setBank(bank);
            credit.setBaseAmount(rs.getBigDecimal("CRD_BSUM"));
            Currency currency = catalogDao.getCurrency(rs.getString("CSH_ID")).get(0);
            credit.setCurrency(currency);
            credit.setCreditDateIssue(rs.getLong("CRD_BDATE"));
            credit.setCreditCurrBody(rs.getBigDecimal("CRD_CBODY"));
            credit.setCreditEnd(rs.getLong("CRD_ENDDATE"));
            String delay = rs.getString("CRD_OUT");
            if (delay.equals("Y")) {
                credit.setOutDelay("Да");
            } else {
                credit.setOutDelay("Нет");
            }
            return credit;
        }
    }
}
