package com.pb.creditbh.service.interfaces;

import com.pb.creditbh.model.entity.Client;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IClientComponent {

    public List<Client> getByInn(String inn);

    public List<Client> getById(long id);

    public List<Client> getByFioAndDateBorn(String firstName, String secondName,
            String lastName, long dateBorn);

    public List<Client> getByDataPassport(String sPass, String nPass);

    public int addClient(Client client) throws SQLException, DataAccessException, InvalidResultSetAccessException;

}
