package com.pb.creditbh.service.interfaces;

import com.pb.creditbh.model.entity.Credit;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface ICreditComponent {

    public List<Credit> getById(long id);
    
    public List<Credit> getByIdCredit(long id);

    public int updateCreditInfo(Credit credit);

    public int addNewCredit(Credit credit);
}
