package com.pb.creditbh.service;

import com.pb.creditbh.model.dao.intefaces.IClientDao;
import com.pb.creditbh.model.dao.intefaces.ICreditDao;
import com.pb.creditbh.model.entity.Client;
import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.service.interfaces.IClientComponent;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
@Service()
public class ClientSpringComponent implements IClientComponent {

    @Autowired
    @Qualifier("clientDaoSpring")
    private IClientDao clientDao;

    @Autowired
    @Qualifier("creditDaoSpring")
    private ICreditDao creditDao;

    @Override
    public synchronized List<Client> getByInn(String inn) {
        List<Client> listClients = clientDao.findByINN(inn);
        if (!listClients.isEmpty()) {
            List<Credit> listCredits = creditDao.getCredits(listClients.get(0).getIdClient());
            listClients.get(0).setListCredits(listCredits);
            System.out.println(listClients.get(0).getLastName());
            return listClients;
        } else {
            return listClients;
        }
    }

    @Override
    public synchronized List<Client> getById(long id) {
        List<Client> listClients = clientDao.findById(id);
        if (!listClients.isEmpty()) {
            List<Credit> listCredits = creditDao.getCredits(listClients.get(0).getIdClient());
            listClients.get(0).setListCredits(listCredits);
            return listClients;
        } else {
            return listClients;
        }
    }

    @Override
    public synchronized List<Client> getByFioAndDateBorn(String firstName, String secondName, String lastName, long dateBorn) {
        List<Client> listClients = clientDao.findByFIOandDateBorn(firstName, secondName, lastName, dateBorn);
        if (!listClients.isEmpty()) {
            List<Credit> listCredits = creditDao.getCredits(listClients.get(0).getIdClient());
            listClients.get(0).setListCredits(listCredits);
            return listClients;
        } else {
            return listClients;
        }
    }

    @Override
    public synchronized List<Client> getByDataPassport(String sPass, String nPass) {
        List<Client> listClients = clientDao.findByDataPassport(sPass, nPass);
        if (!listClients.isEmpty()) {
            List<Credit> listCredits = creditDao.getCredits(listClients.get(0).getIdClient());
            listClients.get(0).setListCredits(listCredits);
            return listClients;
        } else {
            return listClients;
        }
    }

    @Override
    public synchronized int addClient(Client client) throws SQLException, DataAccessException, InvalidResultSetAccessException {
        int result = 0;
        if (clientDao.findByINN(client.getInn()).isEmpty()
                && clientDao.findByDataPassport(client.getSerialPassport(),
                        client.getNumberPassport()).isEmpty()) {
            result = clientDao.save(client);
        }
        return result;
    }

}
