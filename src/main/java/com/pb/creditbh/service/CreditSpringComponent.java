package com.pb.creditbh.service;

import com.pb.creditbh.model.dao.CatalogDao;
import com.pb.creditbh.model.dao.intefaces.ICreditDao;
import com.pb.creditbh.model.entity.Credit;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import com.pb.creditbh.service.interfaces.ICreditComponent;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
@Service("creditSpringComponent")
public class CreditSpringComponent implements ICreditComponent {

    @Autowired
    @Qualifier("creditDaoSpring")
    private ICreditDao creditDao;

    @Autowired
    private CatalogDao catalogDao;

    @Override
    public synchronized List<Credit> getById(long id) {
        List<Credit> credits = creditDao.getCredits(id);
        return credits;
    }

    @Override
    public synchronized List<Credit> getByIdCredit(long id) {
        List<Credit> credits = creditDao.getCreditById(id);
        return credits;
    }

    @Override
    public synchronized int updateCreditInfo(Credit credit) {
        int result = 0;
        result = creditDao.update(credit);
        return result;
    }

    @Override
    public synchronized int addNewCredit(Credit credit) {
        int result = 0;
        Bank newBank = catalogDao.getBankByName(credit.getBank().getBankName()).get(0);
        Currency newCurrency = catalogDao.getCurrencyByName(credit.getCurrency().getCurrName()).get(0);
        credit.setBank(newBank);
        credit.setCurrency(newCurrency);
        credit.setCreditCurrBody(credit.getBaseAmount());
        credit.setOutDelay("N");
        result = creditDao.save(credit);
        return result;
    }

}
