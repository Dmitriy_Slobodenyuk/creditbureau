package com.pb.creditbh.service;

import com.pb.creditbh.model.dao.CatalogDao;
import com.pb.creditbh.model.entity.catalog.Bank;
import com.pb.creditbh.model.entity.catalog.Currency;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("catalogSpringComponent")
public class CatalogSpringComponent {

    @Autowired
    private CatalogDao catalogDao;

    public synchronized int addBank(Bank bank) {
        int result = 0;
        result = catalogDao.addNewBank(bank);
        return result;
    }

    public synchronized int addCurrency(Currency currency) {
        int result = 0;
        result = catalogDao.addNewCurrency(currency);
        return result;
    }

    public synchronized List<Bank> getBanks() {
        return catalogDao.getAllBanks();
    }

    public synchronized List<Currency> getCurrencies() {
        return catalogDao.getAllCurrency();
    }
}
