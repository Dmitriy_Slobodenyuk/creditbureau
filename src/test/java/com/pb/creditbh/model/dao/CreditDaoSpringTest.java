package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.entity.Credit;
import java.math.BigDecimal;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Slobodenyuk D.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
public class CreditDaoSpringTest {

    @Autowired
    CreditDaoSpring creditDaoSpring;

    public CreditDaoSpringTest() {
    }

    @Test
    public void testGetCredits() {
        List<Credit> resultList = creditDaoSpring.getCredits(1);
        boolean expIsEmpty = false;
        boolean resIsEmpty = resultList.isEmpty();
        assertEquals(expIsEmpty, resIsEmpty);
    }
    
    @Test
    public void testGetCreditById() {
        List<Credit> credit = creditDaoSpring.getCreditById(4);
        String expBankCode = "ПБ";
        String resBankCode = credit.get(0).getBank().getBankCode().trim();
       
        List<Credit> resultList = creditDaoSpring.getCreditById(16);
        boolean expIsEmpty = true;
        boolean resIsEmpty = resultList.isEmpty();

        assertEquals(expBankCode, resBankCode);
        assertEquals(expIsEmpty, resIsEmpty);
    }
    
    @Test
    public void testUpdate() {
        Credit credit = new Credit();
        credit.setIdCredit(1);
        credit.setCreditCurrBody(BigDecimal.valueOf(6100.00));
        credit.setCreditEnd(1488405600000L);
        credit.setOutDelay("Нет");
        int expRes = 1;
        int res = creditDaoSpring.update(credit);
        assertEquals(expRes, res);
    }
}
