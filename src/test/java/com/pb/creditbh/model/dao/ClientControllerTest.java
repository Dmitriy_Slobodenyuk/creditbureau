package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.entity.Client;
import com.pb.creditbh.model.entity.ClientWrapper;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import static org.mockito.Mockito.*;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest {

    private MockMvc mockMvc;

    public ClientControllerTest() {
    }

    @Test
    public void testGetClientByIdRest() {
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> listClients = null;
        boolean exp = false;
        boolean result = true;
        int id = 1;
        try {
            clientWrapper = new RestTemplate().getForObject("http://localhost:8080/CreditBH/client/client_by_id/{id}", ClientWrapper.class, id);
            listClients = clientWrapper.getListClients();
            result = listClients.isEmpty();
            assertEquals(exp, result);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetClientByInnRest() {
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> listClients = null;
        boolean exp = false;
        boolean result = true;
        String inn = "2873645362";
        try {
            clientWrapper = new RestTemplate().getForObject(
                    "http://localhost:8080/CreditBH/client/client_by_inn/{inn}",
                    ClientWrapper.class, inn);
            listClients = clientWrapper.getListClients();
            result = listClients.isEmpty();
            assertEquals(exp, result);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetClientByPassportDataRest() {
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> listClients = null;
        boolean exp = false;
        boolean result = true;
        String serial = "АК";
        String number = "837467";
        try {
            clientWrapper = new RestTemplate().getForObject(
                    "http://localhost:8080/CreditBH/client/client_by_passport/{ser}/{num}",
                    ClientWrapper.class, serial, number);
            listClients = clientWrapper.getListClients();
            result = listClients.isEmpty();
            assertEquals(exp, result);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetClientByFioAndDateBornRest() {
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> listClients = null;
        boolean exp = false;
        boolean result = true;
        String firstName = "Дмитрий";
        String secondName = "Васько";
        String lastName = "Андреевич";
        long dateBorn = 539215200000L;
        try {
            clientWrapper = new RestTemplate().getForObject(
                    "http://localhost:8080/CreditBH/client/client_by_fio/{fname}/{sname}/{lname}/{bdate}",
                    ClientWrapper.class, firstName, secondName, lastName, dateBorn);
            listClients = clientWrapper.getListClients();
            result = listClients.isEmpty();
            assertEquals(exp, result);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }
}
