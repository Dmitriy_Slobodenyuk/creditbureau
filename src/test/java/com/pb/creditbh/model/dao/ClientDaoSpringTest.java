/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.creditbh.model.dao;

import com.pb.creditbh.model.entity.Client;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Slobodenyuk D.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
public class ClientDaoSpringTest {

    @Autowired
    ClientDaoSpring clientDaoSpring;

    public ClientDaoSpringTest() {
    }

    @Test
    public void testFindByINN() {
        List<Client> list = clientDaoSpring.findByINN("2873645362");
        String expResult = "Васько";
        String result = list.get(0).getSecondName();

        List<Client> resultList = clientDaoSpring.findByINN("28736453622");
        boolean exp = true;
        boolean res = resultList.isEmpty();

        assertEquals(expResult, result);
        assertEquals(exp, res);
    }

    @Test
    public void testFindById() {
        List<Client> list = clientDaoSpring.findById(1);
        String expResult = "Васько";
        String result = list.get(0).getSecondName();

        List<Client> resultList = clientDaoSpring.findById(11);
        boolean exp = true;
        boolean res = resultList.isEmpty();

        assertEquals(expResult, result);
        assertEquals(exp, res);
    }

    @Test
    public void testFindByFIOandDateBorn() {
        List<Client> list = clientDaoSpring.findByFIOandDateBorn("Дмитрий", "Васько", "Андреевич", 539215200000L);
        String expResult = "Васько";
        String result = list.get(0).getSecondName();

        List<Client> resultList = clientDaoSpring.findByFIOandDateBorn("Дмитрий", "васько", "Андреевич", 539215200000L);
        boolean exp = true;
        boolean res = resultList.isEmpty();

        assertEquals(expResult, result);
        assertEquals(exp, res);
    }

    @Test
    public void testFindByDataPassport() {
        List<Client> list = clientDaoSpring.findByDataPassport("АК", "837467");
        String expResult = "Васько";
        String result = list.get(0).getSecondName();

        List<Client> resultList = clientDaoSpring.findByDataPassport("АК", "8374676");
        boolean exp = true;
        boolean res = resultList.isEmpty();

        assertEquals(expResult, result);
        assertEquals(exp, res);
    }

    @Test
    @Ignore
    public void testSave() {
        Client client = new Client();
        client.setInn("8274637281");
        client.setFirstName("Антон");
        client.setSecondName("Дубцов");
        client.setLastName("Викторович");
        client.setDateBorn(539215200000L);
        client.setSerialPassport("АК");
        client.setNumberPassport("736473");
        int exp = 1;
        int res = clientDaoSpring.save(client);
        assertEquals(exp, res);
    }
}
