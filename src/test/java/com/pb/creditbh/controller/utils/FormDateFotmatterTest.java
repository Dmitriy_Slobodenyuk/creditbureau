/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.creditbh.controller.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Slobodenyuk D.
 */

public class FormDateFotmatterTest {

    private static String dateRegEx = "^((19|20)\\d\\d)-(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])$";
    private static Pattern pattern;

    public FormDateFotmatterTest() {
    }

    @BeforeClass
    public static void testBeforeParse() {
        pattern = Pattern.compile(dateRegEx);
    }

    @Test()
    public void testParseDate() {
        Matcher matcher1 = pattern.matcher("1987-23-02");
        boolean valid1 = matcher1.matches();
        Matcher matcher2 = pattern.matcher("2016-02-12");
        boolean valid2 = matcher2.matches();
        Matcher matcher3 = pattern.matcher("2008-31-03");
        boolean valid3 = matcher3.matches();
        assertTrue("Pattern did ", valid1);
        assertTrue("Pattern did ", valid2);
        assertTrue("Pattern did ", valid3);
    }

}
